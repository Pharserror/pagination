const SETTINGS = {
  // The minimum number of elements to "hide" in the elipses
  elipsesBuffer: 2,
  // The lowest page to start from (is 1 for most cases)
  lowestPage: 1,
  /* The number of page elements we want to render on either side
   * of the current page element
   */
  pageBuffer: 3,
  perPage: 50,
};

/* In order to render the right links for the pagination we need to
 * calculate a few things based on the total number of records
 * Here is a legend for the pagination component
 * The elipses include the possibleRangeMin                       and the possibleRangeMax
 * (<- previous) [lowest page] ... [range start (RS)][RS + 1][...][range end] ... [highest page] (next ->)
 *                Between the range start and the range end is the page range
 * The way this pagination works is: we always want to show a minimum number
 * of elements for the user to click on plus the start and end pages. However,
 * when our range is close to the start or end we want to show those elements
 * as well; specifically, for the current implementation: if we are only
 * hiding 1 element with the elipses we will show that element instead.
 * @param recordsCount [Number] The total number of records we want to paginate
 * @param search [String] The window.location.search
 * returns [Object] An object with the following keys:
 * numPages: Total number of pages
 * page: current page
 * isPageParamPresent: we want to know when generating page links if there
 * is a param already in the url that we can replace or if we tack it on
 * possibleRangeMax: Since we represent some of the pages as "..." (elipses)
 * the possibleRangeMax is what we could show to the user instead of the
 * elipses; in other words, if the elipses are only hiding one page then we
 * want to show that page since it is within the possibleRangeMax
 * possibleRangeMin: Same thing as the possibleRangeMax but for the low end
 * of the page range
 * rangeStart: Where the pagination range for the current page starts at
 * rangeEnd: Where the pagination range for the current page ends at
 */
function calculatePageData(
  recordsCount = 0,
  search = window.location.search,
) {
  const {
    elipsesBuffer,
    lowestPage,
    pageBuffer,
    perPage,
  } = SETTINGS;

  const numPages = Math.ceil(recordsCount / perPage);
  const pageParam = search.match(/page=(\d+)/);
  const isPageParamPresent = !!pageParam;
  const page = isPageParamPresent ? Number(pageParam[1]) : lowestPage;
  /* This is the maximum page number we want to show for the visible page
   * range when we are not near the last page; if the current page plus
   * the pageBuffer is over the max number of pages then we set this to
   * the maximum number of pages, numPages
   */
  const actualRangeMax = (
    (page + pageBuffer) > numPages
      ? numPages
      : (page + pageBuffer)
  );

  // Same thing as actualRangeMax but for the low end of the page range
  const actualRangeMin = (
    (page - pageBuffer) < lowestPage
      ? lowestPage
      : (page - pageBuffer)
  );

  /* The highest number in the page range could show to the user instead
   * of the elipses; if this is over the maximum number of pages we set
   * it to the maximum number instead
   */
  const possibleRangeMax = (
    (actualRangeMax + elipsesBuffer) > numPages
      ? numPages
      : (actualRangeMax + elipsesBuffer)
  );

  const possibleRangeMin = (
    (actualRangeMin - elipsesBuffer) < lowestPage
      ? lowestPage
      : (actualRangeMin - elipsesBuffer)
  );

  /* The number of page elements besides the first and last page that
   * we want to show to the user
   */
  const rangeSize = (pageBuffer * 2);
  const minNumOfElements = rangeSize + 1;
  const rangeStart = (
    /* First, does the range we are showing to the user have less pages in
     * it than the current page plus the left and right buffers?
     */
    (actualRangeMax - actualRangeMin) < rangeSize
      ? (
        /* if it does then we want to know if what we could show to the user
         * (instead of the elipses) contains the lowest page
         */
        possibleRangeMin <= lowestPage
        // if it does then we just show the lowest page
          ? lowestPage
          : (
            /* if it doesn't at this point then we should be within range of
             * the last page and that's what we want to know here: if there are
             * more elements or not
             */
            (possibleRangeMax - actualRangeMin) >= minNumOfElements
            /* if moving right has many more elements and we haven't hit the
             * lowest page yet then we start the range more to the left
             */
              ? possibleRangeMin
              /* if there aren't many more elements to the right then we've hit
               * the last page and so we start our range to the left of that page
               */
              : (actualRangeMax - rangeSize)
          )
      ) : (
        /* If there are enough elements then we want to know if we are
         * close enough to the lowest page that we don't have to show
         * the elipses
         */
        possibleRangeMin > lowestPage
          // if we aren't then we don't
          ? actualRangeMin
          // but if we are then we do
          : lowestPage
      )
  );

  const rangeEnd = (
    /* First, does the range we are showing to the user have less pages in
     * it than the current page plus the left and right buffers?
     */
    (actualRangeMax - actualRangeMin) < rangeSize
      ? (
        /* if it does then we want to know if what we could show to the user
         * (instead of the elipses) contains the highest page
         */
        possibleRangeMax >= numPages
          // If it does then we show the highest page
          ? numPages
          : (
            /* At this point we likely haven't broken away from the lowest
             * page and are still showing some pages that will get hidden by
             * the elipses; so, we check if there are many more elements we can
             * show to the user
             */
            possibleRangeMax >= rangeSize
              /* If there are then we set the range as high as we can to ensure we
               * show the minimum number of elements plus the lowest page and what
               * will get hidden by the elipses
               */
              ? minNumOfElements
              // If there aren't then we show as high as we know we can
              : possibleRangeMax
          )
      ) : (
        /* If there are enough elements then we want to know if we should be
         * showing the last page in the range or if we aren't close enough
         * to it yet
         */
        possibleRangeMax < numPages
          // If we aren't close enough we just show the normal range
          ? actualRangeMax
          // Otherwise we show the last page in the range
          : numPages
      )
  );

  return {
    isPageParamPresent,
    numPages,
    page,
    possibleRangeMax,
    possibleRangeMin,
    rangeStart,
    rangeEnd
  };
}

/* The parent pagination component
 * @param recordsCount [Number] The total number of records we are paginating
 * @returns [Function]
 */
export default function Pagination(recordsCount) {
  const { lowestPage } = SETTINGS;
  const nodes = [];
  const { pathname, search } = window.location;
  const {
    isPageParamPresent,
    numPages,
    page,
    possibleRangeMax,
    possibleRangeMin,
    rangeStart,
    rangeEnd
  } = calculatePageData(recordsCount, search);

  let route;

  // First, if applicable we push on the previous button
  if (numPages > 1) {
    // Pagination button
    nodes.push({
      pathname,
      isDisabled: page === lowestPage,
      search: newSearchPage(isPageParamPresent, page - 1, page),
      text: '<',
    });
  }

  /* Then we loop over the number of pages in the page range and push
    * those onto the element nodes array
    */
  for (let i = rangeStart; i <= rangeEnd; i++) {
    let newSearch = newSearchPage(isPageParamPresent, i, page);
    route = `${pathname}${newSearch}`;

    /* If we are rendering the lowest page link and we are far enough
      * from the lowest page then we render the lowest page link and
      * some elipses
      */
    if (i === rangeStart && possibleRangeMin > lowestPage) {
      let lowestPageSearch = newSearchPage(isPageParamPresent, lowestPage, page);
      lowestPageRoute = `${pathname}${lowestPageSearch}`;

      // Link
      nodes.push({ pageNumber: lowestPage, route: lowestPageRoute });
      nodes.push({ text: '...' });
    }

    // We then push on the current page element
    // Link
    nodes.push({ page, route, pageNumber: i });

    // Same thing as above but for the high end of the page range
    if (i === rangeEnd && possibleRangeMax < numPages) {
      let highestPageSearch = newSearchPage(isPageParamPresent, numPages, page);
      highestPageRoute = `${pathname}${highestPageSearch}`;

      nodes.push({ text: '...' });
      // Link
      nodes.push({ pageNumber: numPages, route: highestPageRoute });
    }
  }

  // Last we push on the next button
  if (numPages > 1) {
    nodes.push({
      pathname,
      isDisabled: page === numPages,
      search: newSearchPage(isPageParamPresent, page + 1, page),
      text: '>',
    });
  }

  // Finally we render the thing
  return (
    <div className="pagination">
      {nodes}
    </div>
  );
}
/* We need to generate the right links for the page elements and in order
 * to do that we have to modify the params those links will have with the
 * right page; this does that
 * @param isPageParamPresent [Boolean] Flag indicating if we have a page
 * param currently in the url
 * @param newPage [Number] The number that the page param should have
 * @param page [Number] The page param the search currently has
 * @returns [String]
 */
function newSearchPage(isPageParamPresent, newPage, page) {
  return (
    window.location.search.length > 0 && isPageParamPresent
      ? window.location.search.replace(`page=${page}`, `page=${newPage}`)
      : `?page=${newPage}`
  );
}
